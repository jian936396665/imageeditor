# ImageEditor

Android画板库，有裁剪、滤镜、色彩亮度调整、边框、纹理、涂鸦笔、Ai风格迁移等功能，所有功能支持强大的撤销、反撤销。

# 特性

- Crop裁剪

画板可以自由裁剪，固定比例裁剪，左右翻转原图，顺时针旋转原图。

- Filter

滤镜，调整，纹理是基于[AndroidGPUImage](https://github.com/cats-oss/android-gpuimage)实现，纹理实现了网络化和自定义纹理。

- 涂鸦画板

画板的每一步操作都支持撤销，画板有一键清除，橡皮擦，和画笔功能。基础画笔可以调整笔触大小和笔触硬度，以及修改笔触效果，自定义笔触。

- AI滤镜

TansferStyle基于[TensorFlow Lite](https://tensorflow.google.cn/lite/tutorials/model_maker_image_classification?hl=zh-cn)实现的。

# Android使用

## Gradle

```
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}

dependencies {
    implementation 'com.gitlab.jian936396665:imageeditor:1.0.97'
}
```

## AndroidManifest

```
<activity
            android:name="com.tuyue.common_sdk.activity.ImagePetternActivity"
            android:hardwareAccelerated="false"/>
```

## 示例

进入编辑图片画板调用：

```
ImageEditor.Companion.get(getCurrentActivity()).isImmersionBar(false)
                        .resultCode(PESDK_EDITOR_RESULT)
                        .start(imageFile.getPath(), exportDir);
```

如果希望在编辑之后得到编辑库操作后的结果图，在Activity中重写OnActivityResult()方法：

```
@Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PESDK_EDITOR_RESULT: {
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        //获取编辑后图片地址
                        String resultPath = data.getStringExtra("image");
                }
            }
        }
    }
```

# 补充

此外，画板桥接了ReactNative，并且在npm持续更新，参考[react-native-ty-image-sdk](https://www.npmjs.com/package/react-native-ty-image-sdk)使用说明。

# Demo下载地址

[Apk下载链接](http://d.3appstore.com/2t6j)