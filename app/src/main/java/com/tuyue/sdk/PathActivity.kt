package com.tuyue.sdk

import android.graphics.Point
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.tuyue.common_sdk.widget.PaletteView
import com.tuyue.core.util.MFBezierCurvesTool
import kotlinx.android.synthetic.main.activity_path.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


/**
 * Create by guojian on 2021/7/5
 * Describe：
 **/
class PathActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_path)
        paletteView.mode = PaletteView.Mode.DRAW

        paletteView.setPenRawSize(20)

        btn_pen.setOnClickListener {
            paletteView.mode = PaletteView.Mode.DRAW
            paletteView.setShadowLayer(0f)
        }
        btn_effect.setOnClickListener {
            paletteView.mode = PaletteView.Mode.PATH_EFFECT
        }
        btn_undo.setOnClickListener {
            paletteView.undo()
        }
        btn_redo.setOnClickListener {
            paletteView.redo()
        }
        btn_strok.setOnClickListener {
            //描边
            paletteView.setShadowLayer(2f)
        }
        btn_clear.setOnClickListener {
            paletteView.clear()
        }
        btn_eraser.setOnClickListener {
            paletteView.mode = PaletteView.Mode.ERASER
        }

    }
}