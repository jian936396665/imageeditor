package com.tuyue.common_sdk.model

import java.io.Serializable


/**
 * Create by guojian on 2021/8/9
 * Describe：
 **/
class UserInfo (
    /**
     * userId: 当前登录用户的id，如果为0，则未登录
    token: 当前登录用户的登录token，用于登录校验
    username: 当前登录的用户昵称
    isMember: 是否会员
    memberExpiredAt: 会员有效期
    avatar: 用户头像
     */
    val userId: String?,
    val token: String?,
    val username: String?,
    var isMember: Boolean = false,
    var memberExpiredAt: String?,
    val avatar: String?
        ): Serializable