package com.tuyue.common_sdk.model

import android.graphics.Bitmap
import com.divyanshu.draw.widget.MyPath
import com.divyanshu.draw.widget.PaintOptions
import com.tuyue.common_sdk.image_edit.PetternType
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilterGroup
import java.util.LinkedHashMap


/**
 * Create by guojian on 2021/4/8
 * Describe：撤销/重置数据类
 **/
class PetternUndoRedoModel (
    //一级菜单类型
    var type: PetternType,
    //滤镜
    var filter: GPUImageFilterGroup = GPUImageFilterGroup(),
    //原图
    var bitmap: Bitmap? = null,
    //边框
    var frameAssetsModel: FrameAssetsModel? = null,
    //笔刷paths
    var paths: Bitmap? = null
        )
