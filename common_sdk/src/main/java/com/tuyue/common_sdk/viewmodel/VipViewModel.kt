package com.tuyue.common_sdk.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.tuyue.core.network.ApiService
import com.tuyue.core.network.RetrofitFactory
import com.tuyue.core.resbody.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.FormBody
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody


/**
 * Create by guojian on 2021/2/25
 * Describe：
 **/
class VipViewModel: ViewModel() {

    var mVipinfo = MutableLiveData<List<VipResModel>>()
    var mUserVipInfo = MutableLiveData<UserVipStatus>()
    var mOrderResult = MutableLiveData<OrderResultResBody>()

    fun getVipPayList(success: MutableLiveData<List<VipResModel>>? = null){
        viewModelScope.launch {
            try {
                val data = withContext(Dispatchers.IO) {
                    RetrofitFactory.instance.getService(ApiService::class.java)
                        .getVipPayList()
                }
                mVipinfo.value = data.data
                success?.value = data.data

            } catch (e: Exception) {
                e.printStackTrace()
                Log.i("请求失败", "${e.message}")
            }
        }
    }

    fun getMemberInfo(
        userId: String?,
        token: String?,
        success: MutableLiveData<UserVipStatus>? = null
    ){
        val map = hashMapOf<String, Any>()
        userId?.let {
            map["userId"] = it
        }
        token?.let {
            map["token"] = it
        }
        val json = Gson().toJson(map)
        val body = RequestBody.create("application/json; charset=utf-8".toMediaTypeOrNull(),json)

        viewModelScope.launch {
            try {
                val data = withContext(Dispatchers.IO) {
                    RetrofitFactory.instance.getService(ApiService::class.java)
                        .getMemberInfo(body)
                }
                mUserVipInfo.value = data
                success?.value = data

            } catch (e: Exception) {
                e.printStackTrace()
                Log.i("请求失败", "${e.message}")
            }
        }
    }

    fun getOrderResult(userId: String?, token: String?, orderId: String?, tradeNo: String?,
                       fail: MutableLiveData<OrderResultResBody>? = null){
        val map = hashMapOf<String, Any>()
        userId?.let {
            map["userId"] = it
        }
        token?.let {
            map["token"] = it
        }
        orderId?.let {
            map["orderId"] = orderId
        }
        tradeNo?.let {
            map["tradeNo"] = tradeNo
        }
        val json = Gson().toJson(map)
        val body = RequestBody.create("application/json; charset=utf-8".toMediaTypeOrNull(),json)
        viewModelScope.launch {
            try {
                val data = withContext(Dispatchers.IO) {
                    RetrofitFactory.instance.getService(ApiService::class.java)
                        .getOrderResult(body)
                }
                if(data.code == 20000) {
                    mOrderResult.value = data
                }else{
                    fail?.value = data
                }

            } catch (e: Exception) {
                e.printStackTrace()
                Log.i("请求失败", "${e.message}")
            }
        }
    }

}