package com.tuyue.common_sdk.helper;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.View;

import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;

public class SpannableHelper {

    private SpannableStringBuilder mSpannableStringBuilder;

    private SpannableHelper(Builder builder) {
        mSpannableStringBuilder = builder.mSpannableStringBuilder;
    }

    private SpannableStringBuilder getSpannableStringBuilder() {
        return mSpannableStringBuilder;
    }

    public static final class Builder {
        private int index;
        private int textLength;
        private SpannableStringBuilder mSpannableStringBuilder;

        public Builder() {
            mSpannableStringBuilder = new SpannableStringBuilder();
        }

        public Builder text(CharSequence text) {
            if (TextUtils.isEmpty(text)) {
                text = "";
            }
            index = mSpannableStringBuilder.length();
            textLength = text.length();
            mSpannableStringBuilder.append(text);
            return this;
        }

        public Builder color(int color) {
            ForegroundColorSpan colorSpan = new ForegroundColorSpan(color);
            mSpannableStringBuilder.setSpan(colorSpan, index, index + textLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return this;
        }

        public Builder color(String color) {
            return color(Color.parseColor(color));
        }

        public Builder size(int size) {
            AbsoluteSizeSpan absoluteSizeSpan = new AbsoluteSizeSpan(size);
            mSpannableStringBuilder.setSpan(absoluteSizeSpan, index, index + textLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return this;
        }

        public Builder size(Context context, @DimenRes int dimenRes) {
            return size((int) context.getResources().getDimension(dimenRes));
        }

        public Builder bold(boolean bold) {
            if (bold) {
                StyleSpan styleSpan = new StyleSpan(Typeface.BOLD);//粗体
                mSpannableStringBuilder.setSpan(styleSpan, index, index + textLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            return this;
        }


        public Builder image(Context context, int id, int algin) {
            text("image");
            ImageSpan span = new ImageSpan(context, id, algin);
            mSpannableStringBuilder.setSpan(span, index, index + textLength, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            return this;
        }

        /**
         * 下划线
         */
        public Builder underline() {
            UnderlineSpan span = new UnderlineSpan();
            mSpannableStringBuilder.setSpan(span, index, index + textLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return this;
        }


        /**
         * 点击
         *
         * @apiNote 同时设置颜色 {@link #color(int)} 需要放在此方法之后执行
         */
        public Builder click(View.OnClickListener onClickListener) {
            ClickableSpan span = new ClickableSpan() {
                @Override
                public void onClick(@NonNull View widget) {
                    if (onClickListener != null) {
                        onClickListener.onClick(widget);
                    }
                }
            };
            mSpannableStringBuilder.setSpan(span, index, index + textLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return this;
        }

        public Spannable build() {
            return new SpannableHelper(this).getSpannableStringBuilder();
        }
    }
}
