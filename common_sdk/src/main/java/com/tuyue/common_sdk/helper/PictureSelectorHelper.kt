package com.tuyue.common_sdk.helper

import android.app.Activity
import com.luck.picture.lib.PictureSelector
import com.luck.picture.lib.config.PictureConfig
import com.luck.picture.lib.config.PictureMimeType
import com.luck.picture.lib.entity.LocalMedia
import com.luck.picture.lib.listener.OnResultCallbackListener
import com.tuyue.common_sdk.image_edit.ImageEditor
import com.tuyue.common_sdk.image_edit.PictureSelectorImageLoader


/**
 * Create by guojian on 2021/4/21
 * Describe：
 **/
object PictureSelectorHelper {

    fun openGallery(context: Activity, call: Callback){
        PictureSelector.create(context).openGallery(PictureMimeType.ofImage())
            .imageEngine(PictureSelectorImageLoader())
            .selectionMode(PictureConfig.SINGLE)
            .forResult(object : OnResultCallbackListener<LocalMedia> {
                override fun onResult(result: MutableList<LocalMedia>?) {
                    result?.let { mutableList ->
                        call.onResult(mutableList[0].path)
                    }
                }

                override fun onCancel() {

                }
            })
    }

    interface Callback{
        fun onResult(result: String)
    }

}