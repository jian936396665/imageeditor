package com.tuyue.common_sdk.sp

/**
 * sp存储
 */
interface ISharedPreferences {
    /**
     * 获取Editor对象
     */
    fun editor(): IEditor

    /**
     * 获取字符串类型的value
     */
    fun getString(key: String, def: String): String?

    /**
     * 获取int类型的value
     */
    fun getInt(key: String, def: Int): Int

    /**
     * 获取long类型的value
     */
    fun getLong(key: String, def: Long): Long

    /**
     * 获取boolean类型的value
     */
    fun getBoolean(key: String, def: Boolean): Boolean

    /**
     * 获取float类型的字符串
     */
    fun getFloat(key: String, def: Float): Float

    /**
     * 获取所有的键值对
     */
    fun getAll(): Map<String, *>

}