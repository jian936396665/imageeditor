package com.tuyue.common_sdk.activity

import BaseActivity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.gyf.immersionbar.ImmersionBar
import com.tuyue.common_sdk.R
import com.tuyue.common_sdk.adapter.PayTypeAdapter
import com.tuyue.common_sdk.adapter.VipTypeAdapter
import com.tuyue.common_sdk.dialog.VipDialog
import com.tuyue.common_sdk.item_decoration.LinearListItemDecoration
import com.tuyue.common_sdk.model.PayPriceModel
import com.tuyue.common_sdk.model.VipPriceModel
import com.tuyue.common_sdk.viewmodel.VipViewModel
import kotlinx.android.synthetic.main.activity_vip.*


/**
 * Create by guojian on 2021/8/1
 * Describe：
 **/
class VipActivity: BaseActivity() {

    private val mVipTypeAdapter = VipTypeAdapter()
    private val mPayTypeAdapter = PayTypeAdapter()
    private lateinit var netViewModel: VipViewModel
    private val vipTypeList = mutableListOf<VipPriceModel>()
    private val payTypeList = mutableListOf<PayPriceModel>()
    //所选会员类型
    private var mSelectVipTypePosition = 1
    //所选支付类型
    private var mSelectPayTypePosition = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        immersionBar {
//            statusBarColor(R.color.pay_bar_color)
//            navigationBarColor(R.color.pay_bar_color)
//            statusBarDarkFont(true)
//            navigationBarDarkIcon(true)
//        }
//        window.getDecorView()
//            .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
//        window.statusBarColor = ActivityCompat.getColor(this, R.color.pay_bar_color)
//        ImmersionBar.with(this)
////            .statusBarColor(R.color.pay_bar_color)
//            .transparentStatusBar()
//            .statusBarDarkFont(true)
//            .init()
        changeStatusBarColor()

        setContentView(R.layout.activity_vip)
        initBase()
        initEvent()
        loadData()
    }

    private fun loadData() {
        netViewModel = ViewModelProvider(this).get(VipViewModel::class.java)
        netViewModel.getVipPayList()
        netViewModel.mVipinfo.observe(this, Observer { list ->
            Log.e("mVipinfo", list.toString())
            vipTypeList.clear()
            val headerModel = VipPriceModel(VipTypeAdapter.VIP_HEADER)
            headerModel.title = "开通时长"
            vipTypeList.add(headerModel)

            list.forEach {
                vipTypeList.add(VipPriceModel(VipTypeAdapter.VIP_CONTENT, null, it))
            }

            mVipTypeAdapter.setList(vipTypeList)

            if(vipTypeList.size > 1){
                resetRealPayment()
            }
        })
    }

    private fun resetRealPayment(){
        tv_count.text = "¥ ${vipTypeList[mSelectVipTypePosition].data?.price?.toString()}"
    }

    private fun initEvent() {
        mVipTypeAdapter.setOnItemClickListener { adapter, view, position ->
            if(position == 0 )return@setOnItemClickListener
            mSelectVipTypePosition = position
            resetRealPayment()
        }
        mPayTypeAdapter.setOnItemClickListener { adapter, view, position ->
            if(position == 0 )return@setOnItemClickListener
            mSelectPayTypePosition = position
        }
        iv_back.setOnClickListener { finish() }
        tv_show_dialog.setOnClickListener {
            VipDialog().show(supportFragmentManager, "vip")
        }
        tv_pay.setOnClickListener {
            val itemId = vipTypeList[mSelectVipTypePosition].data?.id
            val amount = vipTypeList[mSelectVipTypePosition].data?.price
            val payMethod = payTypeList[mSelectPayTypePosition].payMethod

            val intent = Intent()
            intent.action = "com.sdk.intoVip"
            intent.putExtra("StartPay", "1")
            intent.putExtra("itemId", itemId.toString())
            intent.putExtra("amount", amount.toString())
            intent.putExtra("payMethod", payMethod)
            sendBroadcast(intent)

            finish()
        }
    }

    private fun initBase() {
        //会员类型

        rv_vip_type.layoutManager = LinearLayoutManager(this)
        rv_vip_type.adapter = mVipTypeAdapter
        rv_vip_type.addItemDecoration(LinearListItemDecoration("#f6f6f6", 1))

        //支付类型
        payTypeList.clear()
        val payHeaderModel = PayPriceModel(PayTypeAdapter.PAY_HEADER)
        payHeaderModel.title = "支付方式"
        payTypeList.add(payHeaderModel)
        payTypeList.add(PayPriceModel(PayTypeAdapter.PAY_CONTENT, "支付宝", R.drawable.icon_alipay, "alipay"))
        payTypeList.add(PayPriceModel(PayTypeAdapter.PAY_CONTENT, "微信", R.drawable.icon_wechat_pay, "wechat"))
        rv_pay_type.layoutManager = LinearLayoutManager(this)
        rv_pay_type.adapter = mPayTypeAdapter
        mPayTypeAdapter.setList(payTypeList)
        rv_pay_type.addItemDecoration(LinearListItemDecoration("#f6f6f6", 1))
    }

}