package com.tuyue.common_sdk.activity

//import com.hjq.toast.ToastUtils

import BaseActivity
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.KeyEvent
import androidx.lifecycle.ViewModelProvider
import com.luck.picture.lib.tools.ToastUtils
import com.tuyue.common_sdk.R
import com.tuyue.common_sdk.helper.SpannableHelper
import com.tuyue.common_sdk.image_edit.ImageEditor
import com.tuyue.common_sdk.image_edit.ImageEditor.Companion.get
import com.tuyue.common_sdk.image_edit.ImageEditorConfig
import com.tuyue.common_sdk.model.EVENT_REFRESH_PAY_RESULT
import com.tuyue.common_sdk.model.MessageEvent
import com.tuyue.common_sdk.tools.*
import com.tuyue.common_sdk.viewmodel.VipViewModel
import com.tuyue.common_sdk.widget.OnContralerStateLisener
import com.xinlan.imageeditlibrary.editimage.utils.BitmapUtils
import kotlinx.android.synthetic.main.activity_image_pettern.*
import kotlinx.android.synthetic.main.view_draw_layout.view.*
import kotlinx.coroutines.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.File


/**
 * create by guojian
 * Date: 2020/12/7
 * 编辑模版页面
 */
class ImagePetternActivity : BaseActivity() {
    /**
     * 图片本地路径
     */
    private var mUri: String? = null
    var mConfig: ImageEditorConfig? = null
    private var mExportDir: String? = null
    private var mInitTag : String? = null
    private var mBitmap: Bitmap? = null
    private lateinit var netViewModel: VipViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_image_pettern)
        initData()
        initView()
        setEvent()

    }

    private fun setEvent() {
        pettern_expand_controler.setControlerStateListener(object : OnContralerStateLisener {
            override fun finish(bitmap: Bitmap) {
                val fileName = System.currentTimeMillis().toString() + ".jpg"
                val path = BitmapUtil.saveBitmap(
                    this@ImagePetternActivity,
                    bitmap,
                    fileName,
                    mExportDir
                )
                path?.let {
                    intent.putExtra("image", it)
                    setResult(Activity.RESULT_OK, intent)
                }

                finish()
            }

            override fun cancel() {
                setResult(Activity.RESULT_CANCELED)
            }

            override fun payMember() {
                payVip()
            }
        })
    }

    fun payVip(){
        // 显示订单页面
//                ImageEditor.get(this@ImagePetternActivity)
//                    .withOrderInfo("11485", "123", "123","37")
//                    .startPayResult(5)
//        ImageEditor.Companion.get(this).startVipInfo(101)

        val intent = Intent()
        intent.action = "com.sdk.intoVip"
        intent.putExtra("intoVip", "1")
        sendBroadcast(intent)
    }

    private fun initData() {
        EventBus.getDefault().register(this)

        mUri = intent.getStringExtra("uri")
        mExportDir = intent.getStringExtra("exportDir")
        mInitTag = intent.getStringExtra("initTag")
        mUri?.let { uri ->
            mBitmap = BitmapUtils.parseBitmapFromUri(this, uri)
            mBitmap?.let {
                gpuimage.setImageResetMatrix(it)
            }
        }
        val uri = Tools.getImageContentUri(this, File(mUri))
        mConfig = intent.getSerializableExtra("extra") as ImageEditorConfig

        //根据用户信息请求会员状态
        netViewModel = ViewModelProvider(this).get(VipViewModel::class.java)
        mConfig?.mUserInfo?.let {
            netViewModel.getMemberInfo(it.userId, it.token)
        }
        netViewModel.mUserVipInfo.observe(this, { vipInfo ->
            vipInfo?.let {
                mConfig?.loadMemberInfo(it)
            }
        })
    }

    private fun initView() {
        mConfig?.mIsImmersionBar?.let {
            if (it) {
                lifecycle.addObserver(BlackStateBarWindowAdjustPanLifecycle(this))
            }
        }
        pettern_expand_controler.bindGpuImageView(gpuimage, mUri)
        pettern_expand_controler.bindCropImageLayout(crop_image_layout)
        pettern_expand_controler.bindFrameLayout(image_frame_layout)
        pettern_expand_controler.bindDoodleLayout(draw_layout, doodle_adjuster)

    }

    override fun onStart() {
        super.onStart()
        mInitTag?.let {
            val initPosition = when(it){
                "filter" -> 1
                "adjust" -> 2
                "frame" -> 3
                "texture" -> 4
                "brush" -> 5
                "focus" -> 6
                "styleTransfer" -> 7
                else -> 0
            }
            pettern_expand_controler.setInitPosition(initPosition)
        }?:let {
            pettern_expand_controler.setInitPosition(-1)
        }
        mConfig?.let {
            pettern_expand_controler.showFilterParams(it.showFilterParams)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onRefreshVipPay(event: MessageEvent?) {
        event?.let {
            if(it.code == EVENT_REFRESH_PAY_RESULT){
                mConfig?.isMember(true)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
         return false
    }

}

