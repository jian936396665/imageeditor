package com.tuyue.common_sdk.image_edit

import android.graphics.Bitmap
import com.divyanshu.draw.widget.MyPath
import com.divyanshu.draw.widget.PaintOptions
import com.tuyue.common_sdk.model.*
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilterGroup
import java.util.LinkedHashMap

/**
 * create by guojian
 * Date: 2020/12/31
 * 多管道滤镜帮助类
 */
interface IFilterGroupHelper_v2 {

    /**
     * 顺序添加滤镜
     */
    fun addFilter(filterModel: GPUImageModel, petternType: PetternType = PetternType.FILTER, position: Int? = null)

    fun addAdjust(filterModel: GPUImageModel, petternType: PetternType = PetternType.FILTER, position: Int? = null)


    /**
     * 重制撤销
     */
    fun redo(): PetternUndoRedoModel_v2?

    /**
     * 撤销
     */
    fun undo(): PetternUndoRedoModel_v2?

    /**
     * 是否可以撤销
     */
    fun isCanUndo(): Boolean

    /**
     * 是否可以重置
     */
    fun isCanRedo(): Boolean

    /**
     * 记录状态
     */
    fun recordUndoRedoState(petternType: PetternType = PetternType.FILTER, bitmap: Bitmap? = null, frameAssetsModel: FrameAssetsModel? = null,
                          drawBitmap: Bitmap? = null
    )

    /**
     * 记录菜单初始化位置
     */
    fun recordPositionByType(petternType: PetternType, position: Int, hasProgressBar: Boolean)

    /**
     * 获取某个类型菜单上一次操作位置
     */
    fun getLastPositionByType(petternType: PetternType): SelectRecordModel?

    /**
     * 获取上一次滤镜进度条状态
     */
    fun getLastFilterProgress(filter: GPUImageFilter): FilterProgressState?

    fun clearFilterByType(petternType: PetternType)

    fun clearAllState()

    fun getFilterGroup():GPUImageFilter

    /**
     * 是否可以二级撤销
     */
    fun isCanSecondUndo(): Boolean

    /**
     * 是否可以二级重置
     */
    fun isCanSecondRedo(): Boolean

    /**
     * 记录二级状态
     */
    fun recordSecondUndoRedoState(petternType: PetternType = PetternType.FILTER, bitmap: Bitmap? = null, frameAssetsModel: FrameAssetsModel? = null,
                            drawBitmap: Bitmap? = null
    )

    /**
     * 重制二级撤销
     */
    fun redoSecond(): PetternUndoRedoModel_v2?

    /**
     * 二级撤销
     */
    fun undoSecond(): PetternUndoRedoModel_v2?

    fun clearSecondStack()

    fun getLastUndoState(): PetternUndoRedoModel_v2?

    fun getLastRedoState(): PetternUndoRedoModel_v2?

    fun backLastState()

    fun resetRedoState()

}