package com.tuyue.common_sdk.image_edit

import com.tuyue.common_sdk.model.UserInfo
import com.tuyue.core.resbody.UserVipStatus
import java.io.Serializable


/**
 * create by guojian
 * Date: 2020/12/7
 */
class ImageEditorConfig: Serializable {

    /**
     * 本地uri
     */
    var uri: String? = null

    /**
     * 处理图片的类型
     */
    var mType : ImageEditor.EditorType? = ImageEditor.EditorType.EDIT

    /**
     * 支持沉浸式状态栏
     */
    var mIsImmersionBar = false

    /**
     * 导出路径
     */
    var mExportDir: String? = null

    /**
     * 裁剪 crop ，滤镜 filter ，调整 adjust ，边框 frame ，纹理 texture ，笔刷 brush ，焦点 focus ，风格化 styleTransfer
     */
    var initTag: String? = null

    /**
     * 用户信息
     */
    var mUserInfo: UserInfo? = null

    /**
     * 订单id
     */
    var orderId: String? = null

    /**
     * 订单交易号
     */
    var tradeNo: String? = null

    var token: String? = null

    var userId: String? = null

    fun isMember(): Boolean{
        mUserInfo?.isMember?.let {
            return it
        }?:let {
            return false
        }
    }

    fun isMember(isMember: Boolean){
        mUserInfo?.isMember = isMember
    }

    /**
     * 更新会员信息
     */
    fun loadMemberInfo(member: UserVipStatus){
        mUserInfo?.isMember = member.isPremiumMember
        mUserInfo?.memberExpiredAt = member.expiredAt
    }

    /**
     * 是否显示调试滤镜参数
     */
    var showFilterParams: Boolean = false
}