package com.tuyue.common_sdk.image_edit
import android.app.Activity
import android.content.Intent
import com.tuyue.common_sdk.activity.ImagePetternActivity
import com.tuyue.common_sdk.activity.PayResultActivity
import com.tuyue.common_sdk.activity.VipActivity
import com.tuyue.common_sdk.model.UserInfo
import com.tuyue.common_sdk.sp.SPManager


/**
 * create by guojian
 * Date: 2020/12/7
 */
class ImageEditor private constructor(private val mActivity: Activity) {

    private val mConfig: ImageEditorConfig = ImageEditorConfig()
    private var mResultCode = 0

    /**
     * 设置类型
     */
    fun ofType(type: EditorType): ImageEditor {
        mConfig.mType = type
        return this
    }

    /**
     * 沉浸式状态栏
     */
    fun isImmersionBar(immersionBar: Boolean): ImageEditor{
        mConfig.mIsImmersionBar = immersionBar
        return this
    }

    fun setUserInfo(userInfo: UserInfo): ImageEditor{
        mConfig.mUserInfo = userInfo
        return this
    }

    fun setUserInfo(userId: String? = null, token: String? = null, username: String? = null,
                    isMember: Boolean = false, memberExpiredAt: String? = null, avatar: String? = null): ImageEditor{
        val userInfo = UserInfo(userId, token, username, isMember, memberExpiredAt, avatar)
        mConfig.mUserInfo = userInfo
        return this
    }

    fun setParams(opts: HashMap<String, Any>): ImageEditor{
        /**
         * opts 结构：
         * path: 图片路径
         * pageType: 初始进入的页面
         * userId: 当前登录用户的id，如果为0，则未登录
         * token: 当前登录用户的登录token，用于登录校验
         * username: 当前登录的用户昵称
         * isMember: 是否会员
         * memberExpiredAt: 会员有效期
         * avatar: 用户头像
         */
        val userId: String = opts["userId"].toString()
        val token: String = opts["token"].toString()
        val username: String = opts["username"].toString()
        val isMember: Boolean = opts["isMember"] as Boolean
        val memberExpiredAt: String = opts["memberExpiredAt"].toString()
        val avatar: String = opts["avatar"].toString()
        val userInfo = UserInfo(userId, token, username, isMember, memberExpiredAt, avatar)
        mConfig.mUserInfo = userInfo

        SPManager.get(SPManager.USER).editor().putString("userId", userId).commit()
        SPManager.get(SPManager.USER).editor().putString("token", token).commit()
        SPManager.get(SPManager.USER).editor().putBoolean("isMember", isMember).commit()
        return this
    }

    fun start(uri: String, exportDir: String? = null){
        start(uri, exportDir, null, false)
    }

    fun start(uri: String, exportDir: String? = null, initTag: String? = null, showFilterParams: Boolean = false){
        mConfig.uri = uri
        mConfig.initTag = initTag
        mConfig.showFilterParams = showFilterParams
        exportDir?.let {
            mConfig.mExportDir = it
        }
        intoImageEditor()
    }

    fun resultCode(code: Int): ImageEditor{
        mResultCode = code
        return this
    }

    /**
     * 会员支付
     */
    fun startVipInfo(requestCode: Int){
        val intent = Intent(mActivity, VipActivity::class.java)
        mActivity.startActivityForResult(intent, requestCode)
    }

    /**
     * 启动支付结果页
     */
    fun startPayResult(requestCode: Int){
        val intent = Intent(mActivity, PayResultActivity::class.java)
        mConfig.orderId?.let {
            intent.putExtra("orderId", it)
            intent.putExtra("tradeNo", mConfig.tradeNo)
            intent.putExtra("token", mConfig.token)
            intent.putExtra("userId", mConfig.userId)
            mActivity.startActivityForResult(intent, requestCode)
        }
    }

    fun withOrderInfo(orderId: String, tradeNo: String, token: String, userId: String): ImageEditor {
        mConfig.orderId = orderId
        mConfig.tradeNo = tradeNo
        mConfig.token = token
        mConfig.userId = userId
        return this
    }

    private fun intoImageEditor(){
        val intent = Intent(mActivity, ImagePetternActivity::class.java)
        mConfig.uri?.let {
            intent.putExtra("uri", it)
        }
        mConfig.mExportDir?.let {
            intent.putExtra("exportDir", it)
        }
        mConfig.initTag?.let {
            intent.putExtra("initTag", mConfig.initTag)
        }
        intent.putExtra("extra", mConfig)
        mActivity.startActivityForResult(intent, mResultCode)
    }

    companion object {
        fun get(activity: Activity): ImageEditor {
            return ImageEditor(activity)
        }
    }

    enum class EditorType {
        /**
         * 编辑
         */
        EDIT,

        /**
         * 滤镜
         */
        FILTER
    }

}