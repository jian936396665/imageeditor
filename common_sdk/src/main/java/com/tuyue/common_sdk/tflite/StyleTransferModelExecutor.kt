/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tuyue.common_sdk.tflite

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import android.os.SystemClock
import android.util.Log
import com.tuyue.common_sdk.tflite.model.ModelExecutionResult
import com.tuyue.common_sdk.tools.BitmapUtil
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.gpu.GpuDelegate
import java.io.FileInputStream
import java.io.IOException
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import kotlin.collections.set

@SuppressWarnings("GoodTime")
class StyleTransferModelExecutor(
    context: Context,
    private var useGPU: Boolean = false
) {
    private var gpuDelegate: GpuDelegate? = null
    private var numberThreads = 10

    private val interpreterPredict: Interpreter
    private val interpreterTransform: Interpreter

    private var fullExecutionTime = 0L
    private var preProcessTime = 0L
    private var stylePredictTime = 0L
    private var styleTransferTime = 0L
    private var postProcessTime = 0L

    private lateinit var inputsStyleForPredict: Array<Any>
    private lateinit var inputsContentForPredict: Array<Any>
    private lateinit var outputsForPredictStyle: HashMap<Int, Any>
    private lateinit var outputsForPredictContent: HashMap<Int, Any>
    private var contentBottleneck =
        Array(1) { Array(1) { Array(1) { FloatArray(BOTTLENECK_SIZE) } } }
    private var styleBottleneck =
        Array(1) { Array(1) { Array(1) { FloatArray(BOTTLENECK_SIZE) } } }
    private var styleBottleneckBlended =
        Array(1) { Array(1) { Array(1) { FloatArray(BOTTLENECK_SIZE) } } }
    private lateinit var mBitmap: Bitmap

    init {
        if (useGPU) {
            interpreterPredict = getInterpreter(context, STYLE_PREDICT_FLOAT16_MODEL, true)
            interpreterTransform = getInterpreter(context, STYLE_TRANSFER_FLOAT16_MODEL, true)
        } else {
            interpreterPredict = getInterpreter(context, STYLE_PREDICT_INT8_MODEL, false)
            interpreterTransform = getInterpreter(context, STYLE_TRANSFER_INT8_MODEL, false)
        }
    }

    companion object {
        private const val TAG = "StyleTransferMExec"
        const val STYLE_IMAGE_SIZE = 256
        private const val CONTENT_IMAGE_SIZE = 384
        private const val BOTTLENECK_SIZE = 100
        private const val STYLE_PREDICT_INT8_MODEL = "style_predict_quantized_256.tflite"
        private const val STYLE_TRANSFER_INT8_MODEL = "style_transfer_quantized_384.tflite"
        private const val STYLE_PREDICT_FLOAT16_MODEL = "style_predict_f16_256.tflite"
        private const val STYLE_TRANSFER_FLOAT16_MODEL = "style_transfer_f16_384.tflite"
    }

    fun buildContentBitmap(bitmap: Bitmap){
        mBitmap = bitmap
        val contentImage = TfLiteImageUtils.scaleBitmap(mBitmap, STYLE_IMAGE_SIZE)
        //原图
        val inputContent =
            TfLiteImageUtils.bitmapToByteBuffer(contentImage, STYLE_IMAGE_SIZE, STYLE_IMAGE_SIZE)
        inputsContentForPredict = arrayOf(inputContent)
        outputsForPredictContent = HashMap()
        outputsForPredictContent[0] = contentBottleneck
        // Run for blending
        interpreterPredict.runForMultipleInputsOutputs(
            inputsContentForPredict,
            outputsForPredictContent
        )
    }

    fun selectStyle(
        styleBitmap: Bitmap,
        styleInheritance: Float,
        context: Context
    ): ModelExecutionResult {

        stylePredictTime = SystemClock.uptimeMillis()
        //样图
        val cropStyleBitmap = TfLiteImageUtils.scaleBitmap(styleBitmap, 512)
        val inputStyle =
            TfLiteImageUtils.bitmapToByteBuffer(cropStyleBitmap, STYLE_IMAGE_SIZE, STYLE_IMAGE_SIZE)
        inputsStyleForPredict = arrayOf(inputStyle)
        outputsForPredictStyle = HashMap()
        outputsForPredictStyle[0] = styleBottleneck

        preProcessTime = SystemClock.uptimeMillis() - preProcessTime
        // Run for style
        interpreterPredict.runForMultipleInputsOutputs(
            inputsStyleForPredict,
            outputsForPredictStyle
        )

        val contentBlendingRatio = styleInheritance

//        for (i in 0 until contentBottleneck[0][0][0].size) {
//            contentBottleneck[0][0][0][i] =
//                contentBottleneck[0][0][0][i] * contentBlendingRatio
//        }
//
//        for (i in styleBottleneck[0][0][0].indices) {
//            styleBottleneck[0][0][0][i] =
//                styleBottleneck[0][0][0][i] * (1 - contentBlendingRatio)
//        }

        for (i in styleBottleneckBlended[0][0][0].indices) {
            styleBottleneckBlended[0][0][0][i] =
                contentBottleneck[0][0][0][i] * contentBlendingRatio + styleBottleneck[0][0][0][i] * (1 - contentBlendingRatio)
        }

        stylePredictTime = SystemClock.uptimeMillis() - stylePredictTime
        Log.i("PREDICT", "Style Predict Time to run: $stylePredictTime")

        val targetBitmap = mBitmap
        val outputContentImage = TfLiteImageUtils.scaleBitmap(targetBitmap, CONTENT_IMAGE_SIZE)
        val contentArray =
            TfLiteImageUtils.bitmapToByteBuffer(
                outputContentImage,
                CONTENT_IMAGE_SIZE,
                CONTENT_IMAGE_SIZE
            )

        val inputsForStyleTransfer = arrayOf(contentArray, styleBottleneckBlended)
        val outputsForStyleTransfer = HashMap<Int, Any>()
        val outputImage =
            Array(1) { Array(CONTENT_IMAGE_SIZE) { Array(CONTENT_IMAGE_SIZE) { FloatArray(3) } } }
        outputsForStyleTransfer[0] = outputImage

        styleTransferTime = SystemClock.uptimeMillis()
        interpreterTransform.runForMultipleInputsOutputs(
            inputsForStyleTransfer,
            outputsForStyleTransfer
        )
        //根据混合图片的宽高rgb值重新渲染bitmap
        val styledImage =
            TfLiteImageUtils.convertArrayToBitmap(
                outputImage,
                CONTENT_IMAGE_SIZE,
                CONTENT_IMAGE_SIZE
            )
        //从方形bigmap中裁剪结果
        var cropWidth = 0
        var cropHeight = 0
        if (targetBitmap.width > targetBitmap.height) {
            cropWidth = CONTENT_IMAGE_SIZE
            cropHeight = cropWidth.times(targetBitmap.height).div(targetBitmap.width)
        } else {
            cropHeight = CONTENT_IMAGE_SIZE
            cropWidth = cropHeight.times(targetBitmap.width).div(targetBitmap.height)
        }
        val cropBitmap = TfLiteImageUtils.cropBitmap(styledImage, cropWidth, cropHeight)
        //还原bitmap原尺寸
        val matrix = Matrix()
        matrix.postScale(
            targetBitmap.width.toFloat().div(cropBitmap.width),
            targetBitmap.height.toFloat().div(cropBitmap.height)
        )

        return ModelExecutionResult(
            Bitmap.createBitmap(
                cropBitmap,
                0,
                0,
                cropBitmap.width,
                cropBitmap.height,
                matrix,
                false
            ),
            preProcessTime,
            stylePredictTime,
            styleTransferTime,
            postProcessTime,
            fullExecutionTime,
            formatExecutionLog()
        )
    }

    fun execute(
        targetBitmap: Bitmap,
        styleBitmap: Bitmap,
        context: Context
    ): ModelExecutionResult {
//    try {
        Log.i(TAG, "running models")

        fullExecutionTime = SystemClock.uptimeMillis()
        preProcessTime = SystemClock.uptimeMillis()

        val contentImage = TfLiteImageUtils.scaleBitmap(targetBitmap, CONTENT_IMAGE_SIZE)
        val contentArray =
            TfLiteImageUtils.bitmapToByteBuffer(
                contentImage,
                CONTENT_IMAGE_SIZE,
                CONTENT_IMAGE_SIZE
            )
        val cropStyleBitmap = BitmapUtil.scaleBitmap(styleBitmap, 512)
        val input =
            TfLiteImageUtils.bitmapToByteBuffer(cropStyleBitmap, STYLE_IMAGE_SIZE, STYLE_IMAGE_SIZE)

        val inputsForPredict = arrayOf<Any>(input)
        val outputsForPredict = HashMap<Int, Any>()
        outputsForPredict[0] = styleBottleneckBlended
        preProcessTime = SystemClock.uptimeMillis() - preProcessTime

        stylePredictTime = SystemClock.uptimeMillis()
        // The results of this inference could be reused given the style does not change
        // That would be a good practice in case this was applied to a video stream.
        interpreterPredict.runForMultipleInputsOutputs(inputsForPredict, outputsForPredict)
        stylePredictTime = SystemClock.uptimeMillis() - stylePredictTime
        Log.d(TAG, "Style Predict Time to run: $stylePredictTime")

        val inputsForStyleTransfer = arrayOf(contentArray, styleBottleneckBlended)
        val outputsForStyleTransfer = HashMap<Int, Any>()
        val outputImage =
            Array(1) { Array(CONTENT_IMAGE_SIZE) { Array(CONTENT_IMAGE_SIZE) { FloatArray(3) } } }
        outputsForStyleTransfer[0] = outputImage

        styleTransferTime = SystemClock.uptimeMillis()
        interpreterTransform.runForMultipleInputsOutputs(
            inputsForStyleTransfer,
            outputsForStyleTransfer
        )
        styleTransferTime = SystemClock.uptimeMillis() - styleTransferTime
        Log.d(TAG, "Style apply Time to run: $styleTransferTime")

        postProcessTime = SystemClock.uptimeMillis()
        var styledImage =
            TfLiteImageUtils.convertArrayToBitmap(
                outputImage,
                CONTENT_IMAGE_SIZE,
                CONTENT_IMAGE_SIZE
            )
        postProcessTime = SystemClock.uptimeMillis() - postProcessTime

        fullExecutionTime = SystemClock.uptimeMillis() - fullExecutionTime
        Log.d(TAG, "Time to run everything: $fullExecutionTime")

        var cropWidth = 0
        var cropHeight = 0
        if (targetBitmap.width > targetBitmap.height) {
            cropWidth = CONTENT_IMAGE_SIZE
            cropHeight = cropWidth.times(targetBitmap.height).div(targetBitmap.width)
        } else {
            cropHeight = CONTENT_IMAGE_SIZE
            cropWidth = cropHeight.times(targetBitmap.width).div(targetBitmap.height)
        }

        return ModelExecutionResult(
            TfLiteImageUtils.cropBitmap(styledImage, cropWidth, cropHeight),
            preProcessTime,
            stylePredictTime,
            styleTransferTime,
            postProcessTime,
            fullExecutionTime,
            formatExecutionLog()
        )
//    } catch (e: Exception) {
//      val exceptionLog = "something went wrong: ${e.message}"
//      Log.d(TAG, exceptionLog)
//
//      val emptyBitmap =
//        ImageUtils.createEmptyBitmap(
//          CONTENT_IMAGE_SIZE,
//          CONTENT_IMAGE_SIZE
//        )
//      return ModelExecutionResult(
//        emptyBitmap, errorMessage = e.message!!
//      )
//    }
    }

    @Throws(IOException::class)
    private fun loadModelFile(context: Context, modelFile: String): MappedByteBuffer {
        val fileDescriptor = context.assets.openFd(modelFile)
        val inputStream = FileInputStream(fileDescriptor.fileDescriptor)
        val fileChannel = inputStream.channel
        val startOffset = fileDescriptor.startOffset
        val declaredLength = fileDescriptor.declaredLength
        val retFile = fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
        fileDescriptor.close()
        return retFile
    }

    @Throws(IOException::class)
    private fun getInterpreter(
        context: Context,
        modelName: String,
        useGpu: Boolean = false
    ): Interpreter {
        val tfliteOptions = Interpreter.Options()
        tfliteOptions.setNumThreads(numberThreads)

        gpuDelegate = null
        if (useGpu) {
            gpuDelegate = GpuDelegate()
            tfliteOptions.addDelegate(gpuDelegate)
        }

        tfliteOptions.setNumThreads(numberThreads)
        return Interpreter(loadModelFile(context, modelName), tfliteOptions)
    }

    private fun formatExecutionLog(): String {
        val sb = StringBuilder()
        sb.append("Input Image Size: $CONTENT_IMAGE_SIZE x $CONTENT_IMAGE_SIZE\n")
        sb.append("GPU enabled: $useGPU\n")
        sb.append("Number of threads: $numberThreads\n")
        sb.append("Pre-process execution time: $preProcessTime ms\n")
        sb.append("Predicting style execution time: $stylePredictTime ms\n")
        sb.append("Transferring style execution time: $styleTransferTime ms\n")
        sb.append("Post-process execution time: $postProcessTime ms\n")
        sb.append("Full execution time: $fullExecutionTime ms\n")
        return sb.toString()
    }

    fun close() {
        interpreterPredict.close()
        interpreterTransform.close()
        if (gpuDelegate != null) {
            gpuDelegate!!.close()
        }
    }
}
