package com.tuyue.common_sdk.widget

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.tuyue.common_sdk.R
import com.tuyue.common_sdk.model.FrameAssetsModel
import com.tuyue.common_sdk.tools.DensityUtil
import kotlinx.android.synthetic.main.view_frame_layout.view.*


/**
 * Create by guojian on 2021/6/12
 * Describe：
 **/
class ImageFrameLayout(context: Context, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    init {
        inflate(context, R.layout.view_frame_layout, this)
    }

    fun setFrameResouce(frameResNode: FrameResNode?){
        frame_view.setFrameResouce(frameResNode)
    }

    fun setFrame(frameId: Int?){
        frame_view.setFrame(frameId)
    }

    fun setFrameAssets(
        frameAssetsModel: FrameAssetsModel?,
        isTiled: Boolean = false,
        frameOffset: Int = DensityUtil.dp2Px(
            44f
        )
    ){
        frame_view.setFrameAssets(frameAssetsModel, isTiled, frameOffset)
    }

    fun result():SecondNode?{
        return frame_view.result()
    }

    fun getFrameOffset(): Int{
        return frame_view.getFrameOffset()
    }

    fun setPetternBitmap(bitmap: Bitmap){
        frame_view.setPetternBitmap(bitmap)
        frame_view.post {
            updateDrawMargin(frame_view.getFrameOffset())
        }
    }

    fun setDrawBitmap(bitmap: Bitmap?){
        frame_draw_view.setImageBitmap(bitmap)
    }

    private fun updateDrawMargin(frameOffset: Int){
        //fixme 暂时写死了偏移量
        val mFrameOffset: Int = DensityUtil.dp2Px(44f)
        val params = frame_draw_view.layoutParams as ConstraintLayout.LayoutParams
        params.setMargins(mFrameOffset, mFrameOffset, mFrameOffset, mFrameOffset)
        frame_draw_view.layoutParams = params
    }
}