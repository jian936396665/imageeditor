package com.tuyue.common_sdk.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.tuyue.common_sdk.R
import com.tuyue.common_sdk.model.DialogVipModel


/**
 * Create by guojian on 2021/8/3
 * Describe：
 **/
class DialogVipAdapter: BaseQuickAdapter<DialogVipModel, BaseViewHolder>(R.layout.item_vip_dialog) {

    override fun convert(holder: BaseViewHolder, item: DialogVipModel) {
        holder.setText(R.id.tv_title, item.title)
        holder.setImageResource(R.id.iv_icon, item.icon)
    }
}