package com.tuyue.common_sdk.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.appcompat.app.AppCompatDialog
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tuyue.common_sdk.R
import com.tuyue.common_sdk.adapter.DialogVipAdapter
import com.tuyue.common_sdk.model.DialogVipModel


/**
 * Create by guojian on 2021/8/3
 * Describe：
 **/
class VipDialog: DialogFragment() {

    private val mList = mutableListOf<DialogVipModel>()
    private val mAdapter = DialogVipAdapter()
    private var contentView: View? = null

    init {
        mList.add(DialogVipModel(R.drawable.icon_dialog_vip1, "会员标识"))
        mList.add(DialogVipModel(R.drawable.icon_dialog_vip2, "专属二维码"))
        mList.add(DialogVipModel(R.drawable.icon_dialog_vip3, "专属高清图库"))
        mList.add(DialogVipModel(R.drawable.icon_dialog_vip4, "会员专属字体"))
        mList.add(DialogVipModel(R.drawable.icon_dialog_vip5, "会员专属模板"))
        mList.add(DialogVipModel(R.drawable.icon_dialog_vip6, "会员专属贴纸"))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.fragment_dialog)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AppCompatDialog(context)
        dialog.setContentView(R.layout.dialog_vip)
        return dialog
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        val window: Window?
        if (dialog != null) {
            window = dialog.window
            if (window != null) {
                window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                val params = window.attributes
                // 使用ViewGroup.LayoutParams，以便Dialog 宽度充满整个屏幕
                params.width = ViewGroup.LayoutParams.MATCH_PARENT
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT
                window.decorView.setPadding(0, 0, 0, 0)
                window.attributes = params
            }
        }
        initData()
    }

    private fun initData() {
        dialog?.let {
            val recyclerView = it.findViewById<RecyclerView>(R.id.rv_dialog_content)
            recyclerView.layoutManager = GridLayoutManager(context, 3)
            recyclerView.adapter = mAdapter
            mAdapter.setList(mList)

            it.findViewById<View>(R.id.tv_close).setOnClickListener {
                dismiss()
            }
        }
    }
}