package com.tuyue.core.network

import com.tuyue.core.resbody.*
import okhttp3.RequestBody
import retrofit2.http.*


/**
 * interface ApiService
 **/
interface ApiService {

    @GET("photo-editor/framelist")
    suspend fun getFrameList(): BaseResp<List<FrameResBody>>

    @GET("photo-editor/texturelist")
    suspend fun getTextureList(): BaseResp<List<TextureResBody>>

    @GET("photo-editor/styletransferlist")
    suspend fun getStyleTransferList(): BaseResp<List<StyleTransferResBody>>

    @GET("photo-editor/brushtexturelist")
    suspend fun getBrushList(): BaseResp<List<BrushResBody>>

    /**
     * 获取会员item价格列表
     */
    @GET("members/premium/items")
    suspend fun getVipPayList(): BaseResp<List<VipResModel>>

    /**
     * 会员状态查询
     */
    @POST("premium-member/info")
    suspend fun getMemberInfo(@Body body: RequestBody): UserVipStatus

    /**
     * 订单结果查询
     */
    @POST("premium-member/order/result")
    suspend fun getOrderResult(@Body body: RequestBody): OrderResultResBody


}