package com.tuyue.core.resbody


/**
 * Create by guojian on 2021/7/26
 * Describe：
 **/
class BrushResBody (
    val brushTextureID: Int,
    val brushTextureType: String,
    val brushTextureUpdateTime: Long,
    val brushTextureTitle: String,
    val brushTextureIcon: String,
    val brushTextureZip: String,
    val brushTextureZipMd5: String,
    //是否随机展示素材笔刷，0不随机，1为随机
    val showTextureRandom: Int,
    //是否为会员资源 值为0 不是， 值为1是
    val vipResources: Int
        )