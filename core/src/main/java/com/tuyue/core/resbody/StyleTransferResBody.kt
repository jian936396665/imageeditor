package com.tuyue.core.resbody

data class StyleTransferResBody(
    val styletransferID: Int,
    val styletransferTime: Long,
    val styletransferTitle: String,
    val styletransferIcon: String,
    val styletransferZip: String,
    val styletransferZipMd5: String,
    val styletransferBlendingRatio: Float,
    //是否为会员资源 值为0 不是， 值为1是
    val vipResources: Int
)
