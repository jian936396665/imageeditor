package com.tuyue.core.resbody


/**
 * Create by guojian on 2021/8/12
 * Describe：
 **/
class OrderResultResBody (
    var code: Int,
    var info:Info
        )

class Info(
    var amount:Int,
    var expiredAt: String,
    var payMethod:String
)